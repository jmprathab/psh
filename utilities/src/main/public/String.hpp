#pragma once

#ifdef _WIN32
#ifdef UTILITIES_MODULE_EXPORT
#define UTILITIES_API __declspec(dllexport)
#else
#define UTILITIES_API __declspec(dllimport)
#endif
#else
#define UTILITIES_API
#endif

#include <stddef.h>

namespace Utilities
{

UTILITIES_API class String
{
public:
	String(char *string);
	~String();
	size_t length();

private:
	static size_t getLength(char *data);
	char *mData;
};
} // namespace Utilities