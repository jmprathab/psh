#include <simplepsh.hpp>

#include <cstdlib>
#include <memory>

#include "interpreter.hpp"
#include "noopinterpreter.hpp"

class PSH;

int main(int argc, char **argv)
{
	std::unique_ptr<Interpreter> interpreter(new NoOpInterpreter());
	std::unique_ptr<PSH> psh(new SimplePSH(std::move(interpreter)));
	psh->initialize();
	psh->execute();
	psh->terminate();
	return EXIT_SUCCESS;
}