#include "simplepsh.hpp"

#include <iostream>
#include <string>

#include "command.hpp"
#include "interpreter.hpp"

SimplePSH::SimplePSH(std::unique_ptr<Interpreter> interpreter)
	: PSH(std::move(interpreter))
{
	// Nothing to do here
}

SimplePSH::~SimplePSH()
{
	// Nothing to do here
}

void SimplePSH::execute()
{
	std::cout << "Simple PSH : Interpreting commands\n";
	while (true)
	{
		std::cout << ">>";
		std::string userInput;
		getline(std::cin, userInput);
		if (0 == userInput.compare("exit"))
		{
			return;
		}
		std::unique_ptr<Command> userCommand(new Command(userInput));
		mInterpreter->interpret(std::move(userCommand));
	}
}
