#pragma once
#include "psh.hpp"

class SimplePSH : public PSH
{
public:
	SimplePSH(std::unique_ptr<Interpreter> interpreter);
	virtual ~SimplePSH();
	virtual void execute() override;
};