#pragma once

#include "interpreter.hpp"

class NoOpInterpreter : public Interpreter
{
public:
    NoOpInterpreter();
    ~NoOpInterpreter();
    virtual void interpret(std::unique_ptr<Command> command) override;
};