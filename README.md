# psh

psh is a simple object-oriented shell implementation.

## Installation

psh is built using Gradle.

```bash
gradlew build
```

## Usage

Execute test script from the project root directory.

```bash
test.sh # On Unix
test.bat # On Windows
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)
