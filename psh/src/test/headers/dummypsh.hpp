#pragma once

#include "psh.hpp"

class DummyPSH : public PSH
{
public:
    DummyPSH(std::unique_ptr<Interpreter> interpreter);
    virtual ~DummyPSH();
    virtual void initialize() override;
    virtual void execute() override;
    virtual void terminate() override;
};