#pragma once

#include "interpreter.hpp"
#include "command.hpp"

class DummyInterpreter : public Interpreter
{
public:
	DummyInterpreter();
	virtual ~DummyInterpreter();
	virtual void interpret(std::unique_ptr<Command> command) override;
};