#include "dummypsh.hpp"

#include <iostream>

DummyPSH::DummyPSH(std::unique_ptr<Interpreter> interpreter)
    : PSH(std::move(interpreter))
{
    // Nothing to do here
}

DummyPSH::~DummyPSH()
{
    // Nothing to do here
}

void DummyPSH::initialize()
{
    std::cout << "Initializing DummyPSH\n";
}

void DummyPSH::terminate()
{
    std::cout << "Terminating DummyPSH\n";
}

void DummyPSH::execute()
{
    std::cout << "Start executing DummyPSH\n";
    std::unique_ptr<Command> command(new Command("This is a test command from DummyPSH"));
    mInterpreter->interpret(std::move(command));
    std::cout << "Finished executing DummyPSH\n";
}