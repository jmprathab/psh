#include <cstdlib>

#include "psh.hpp"
#include "dummyinterpreter.hpp"
#include "dummypsh.hpp"

int main(int argc, char **argv)
{
    std::unique_ptr<Interpreter> interpreter(new DummyInterpreter());
    std::unique_ptr<PSH> psh(new DummyPSH(std::move(interpreter)));

    psh->initialize();
    psh->execute();
    psh->terminate();

    return EXIT_SUCCESS;
}