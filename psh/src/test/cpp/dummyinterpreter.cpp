#include "dummyinterpreter.hpp"

#include <iostream>

DummyInterpreter::DummyInterpreter()
{
	// Nothing to do here
}

DummyInterpreter::~DummyInterpreter()
{
	// Nothing to do here
}

void DummyInterpreter::interpret(std::unique_ptr<Command> command)
{
	std::cout << "Dummy Interpreter : Executing command : " << command->getCommand() << "\n";
}