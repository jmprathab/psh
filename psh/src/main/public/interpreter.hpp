#pragma once

#include <memory>

#include "command.hpp"

class Interpreter
{
public:
	Interpreter();
	virtual ~Interpreter();
	virtual void interpret(std::unique_ptr<Command> command) = 0;
};