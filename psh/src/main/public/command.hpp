#pragma once

#include <string>

class Command
{
public:
	Command(std::string command);
	std::string getCommand();

private:
	std::string mCommand;
};