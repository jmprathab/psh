/**
// Defines the interface for shell implementation
// This is the base class, any new shell should inherit this class
**/
#pragma once

#include <memory>

#include <interpreter.hpp>

class PSH
{
public:
	PSH(std::unique_ptr<Interpreter> interpreter);
	virtual ~PSH();
	virtual void initialize();
	virtual void execute() = 0;
	virtual void terminate();

protected:
	std::unique_ptr<Interpreter> mInterpreter;
};
